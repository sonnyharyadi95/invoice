const BASE_URL = "http://localhost:8000/";
const end_point = [['customer', 'inpCustomer'], ['item', 'inpItem_0']];
const itemInput = ['inpItem', 'inpQtyItem', 'inpItemPrice', 'inpAmountItem', 'inpIdDtItem'];

const onSelectFunc = (obj) => {
    let value = obj.options[obj.selectedIndex].value.split("_");
    // let actualValue = value[0];
    let address, country = "";
    let unitPrice = 0;
    if (obj.id == "inpCustomer") {
        address = value[1];
        country = value[2];
        document.getElementById("inpCustomerAddress").value = address;
        document.getElementById("inpCustomerCountry").value = country;
    }
    else {
        unitPrice = value[1];
        let indexSelected = obj.id.split("_")[1]
        document.getElementById(`inpItemPrice_${indexSelected}`).value = unitPrice;
    }
}

const onChangeInpQty = (obj) => {
    let indexSelected = obj.id.split("_")[1]
    let itemPrice = document.getElementById(`inpItemPrice_${indexSelected}`).value;
    itemPrice = itemPrice.length == 0 ? 0 : parseFloat(itemPrice);
    document.getElementById(`inpAmountItem_${indexSelected}`).value = (obj.value * itemPrice).toFixed(2);
}

const populateSelectData = (endPoint, divId) => {
    let custUrl = `${BASE_URL}/${endPoint}`;

    fetch(custUrl)
        .then(response => response.json())
        .then((cust) => {
            if (!cust.success) {
                alert('FAIL TO FETCH API')
            }
            cust.data.map((prop) => {
                let option = document.createElement('option');
                option.text = endPoint == "customer" ? prop.name : `[${prop.type}] ${prop.description}`;
                option.value = endPoint == "customer" ? `${prop.id}_${prop.address}_${prop.country}` : `${prop.id}_${prop.unit_price}`;
                const custElement = document.getElementById(divId);
                custElement.appendChild(option)
            });
        })
        .catch((e) => console.log(`FAIL TO FETCH API ${e}`))
}

const appendElement = () => {
    const itemArea = document.getElementById("itemArea");
    const itemWrapper = document.querySelectorAll(".wrapper");
    let divId = itemWrapper[itemWrapper.length - 1].id;
    const itemLastElement = document.getElementById(divId);
    let newElement = itemLastElement.cloneNode(true);
    let newId = `item_${parseInt(divId.split("_")[1]) + 1}`
    newElement.id = newId;
    itemArea.append(newElement);
    const parent = document.querySelector(`#${newElement.id}`).children[1];
    parent.querySelector(".inpItem").id = `inpItem_${newId.split("_")[1]}`;

    parent.querySelector(".inpQtyItem").id = `inpQtyItem_${newId.split("_")[1]}`;
    parent.querySelector(".inpQtyItem").value = null;

    parent.querySelector(".inpItemPrice").id = `inpItemPrice_${newId.split("_")[1]}`;
    parent.querySelector(".inpItemPrice").value = null;

    parent.querySelector(".inpAmountItem").id = `inpAmountItem_${newId.split("_")[1]}`;
    parent.querySelector(".inpAmountItem").value = null;

    parent.querySelector(".inpIdDtItem").id = `inpIdDtItem_${newId.split("_")[1]}`;
    parent.querySelector(".inpIdDtItem").value = null;
}

const saveInvoiceData = () => {
    const itemLength = document.getElementsByClassName("wrapper").length;
    const headerInput = ['inpIssueDate', 'inpDueDate', 'inpCustomer', 'inpPayment'];
    objSave = {
        "issue_date": document.getElementById(headerInput[0]).value,
        "due_date": document.getElementById(headerInput[1]).value,
        "subject": "random",
        "supplier_id": 1,
        "payment": document.getElementById(headerInput[3]).value,
        "customer_id": document.getElementById(headerInput[2]).value.split("_")[0]
    }
    const itemInput = ['inpItem', 'inpQtyItem', 'inpItemPrice', 'inpAmountItem', 'inpIdDtItem'];
    const arrayItem = [];
    let subtotal = 0;
    for (let index = 0; index < itemLength; index++) {
        let objDetailItem = {
            "item_id": document.getElementById(`${itemInput[0]}_${index}`).value.split("_")[0],
            "qty": document.getElementById(`${itemInput[1]}_${index}`).value,
            "subtotal": document.getElementById(`${itemInput[2]}_${index}`).value
        }
        let dtId = document.getElementById(`${itemInput[4]}_${index}`).value;
        if (dtId) {
            objDetailItem.id = dtId;
        }
        arrayItem.push(objDetailItem);
        subtotal += parseFloat(document.getElementById(`${itemInput[2]}_${index}`).value);
    }
    objSave.tax_rate = (subtotal * 0.1).toFixed(2);
    console.log(objSave.tax_rate);
    objSave.item = arrayItem;

    postData(invoiceId, objSave).then((data) => {
        alert(data.message)
    })

}

const postData = async (invoiceId, obj) => {

    let postURL = invoiceId ? `${BASE_URL}update_invoice/${invoiceId}` : BASE_URL;
    const response = await fetch(postURL, {
        method: 'POST',
        mode: 'cors',
        cache: 'no-cache',
        credentials: 'same-origin',
        headers: {
            'Content-Type': 'application/json'
        },
        redirect: 'follow',
        referrerPolicy: 'no-referrer',
        body: JSON.stringify(obj)
    });

    return response.json()
}

const getInvoiceData = async (invoiceId) => {
    let urlInvoice = `${BASE_URL}invoice_list/${invoiceId}`;
    return await fetch(urlInvoice)
        .then(response => response.json())
        .then((datas) => datas.data.map((data) => data))
        .catch((e) => console.log(`FAIL TO FETCH API ${e}`))
}

const setInvoiceData = (invoiceId) => {
    getInvoiceData(invoiceId).then((data) => {
        data = data[0]
        document.getElementById("inpCustomer").value = `${data.customer_id}_${data.cust_address}_${data.cust_country}`;
        document.getElementById("inpCustomerAddress").value = data.cust_address;
        document.getElementById("inpCustomerCountry").value = data.cust_country;
        document.getElementById("inpIssueDate").valueAsDate = new Date(data.issue_date);
        document.getElementById("inpDueDate").valueAsDate = new Date(data.due_date);
        data.item.forEach((element, index) => {
            if (index > 0) {
                appendElement();
            }
            console.log(element);
            const itemInput = ['inpItem', 'inpQtyItem', 'inpItemPrice', 'inpAmountItem', 'inpIdDtItem'];
            document.getElementById(`inpIdDtItem_${index}`).value = element.id;
            document.getElementById(`inpItem_${index}`).value = `${element.item_id}_${element.unit_price}`;
            document.getElementById(`inpQtyItem_${index}`).value = element.qty;
            document.getElementById(`inpItemPrice_${index}`).value = element.unit_price;
            document.getElementById(`inpAmountItem_${index}`).value = element.subtotal;
        })
    });
}

end_point.map((x) => populateSelectData(x[0], x[1]));

const invoiceId = new URL(window.location.href).searchParams.get("invoiceId");
if (invoiceId) {
    setInvoiceData(invoiceId);
}