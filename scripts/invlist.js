const BASE_URL = "http://localhost:8000/";

const getInvoieData = () => {
    const invId = document.getElementById("inpSearchInv");
    let urlInvoice = invId.value ? `${BASE_URL}invoice_list/${invId.value}` : `${BASE_URL}invoice_list`;

    fetch(urlInvoice)
        .then(response => response.json())
        .then((invoices) =>
            invoices.data.map((x) => `
            <tr>
                <td> ${x.id} </td>
                <td> ${x.issue_date} </td>
                <td> ${x.due_date} </td>
                <td> ${x.subject} </td>
                <td> ${x.supplier_id} </td>
                <td> ${x.customer_id} </td>
                <td> 
                    ${x.item.map((items) => '[' + items.type + '] - ' + items.description + ' @' + items.qty + '<br>').join('')}
                </td>
                <td> ${x.total} </td>
                <td> ${x.tax_rate} </td>
                <td> ${x.payment} </td>
                <td> ${x.amount_due} </td>
                <td> <button type="button" onclick="getDataToUpdate(${x.id})"> + </button> <button onclick="deleteInvoice(${x.id})"> - </button>
            </tr>
            `).join("")
        )
        .then((invList) => document.getElementById("dataTable").innerHTML = invList)
        .catch((e) => console.log(`FAIL TO FETCH API ${e}`))
}

const deleteInvoice = async (invoiceId) => {
    let urlDelete = `${BASE_URL}${invoiceId}`;
    await fetch(urlDelete, {
        method: 'DELETE',
        mode: 'cors',
        cache: 'no-cache',
        credentials: 'same-origin',
        headers: {
            'Content-Type': 'application/json'
        },
        redirect: 'follow',
        referrerPolicy: 'no-referrer'
    }).then((result) => result.json())
        .then((response) => {
            alert(response.message)
            if (response.success) {
                getInvoieData();
            }
        });
}

const getDataToUpdate = (invoiceId) => {
    window.location = 'index.html?invoiceId=' + invoiceId;
}